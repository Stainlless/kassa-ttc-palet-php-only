<?php
require_once("dbconfig.php");
require_once ("exceptions.php");

class Rekening
{
    private $rekeningId;
    private $naam;
    Private $bedrag;

    Public function __construct($crekeningId = null, $cnaam = null, $cbedrag = null)
    {
        $this->rekeningId = $crekeningId;
        $this->naam = $cnaam;
        $this->bedrag = $cbedrag;
    }

    public function getRekeningId()
    {
        return $this->rekeningId;
    }

    public function getNaam()
    {
        return $this->naam;
    }

    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    public function getBedrag(): float
    {
        return $this->bedrag;
    }

    public function setBedrag($bedrag)
    {
        $this->bedrag = $bedrag;
    }

    public function ToonRekeningen()
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("SELECT * FROM rekeningen");
        $stmt->execute();
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultSet)) {
            return false;
        }

        $rekeningenLijst = array();

        foreach ($resultSet as $rekening) {
            $rekeningObj = new rekening($rekening["rekeningId"], $rekening["naam"], $rekening["bedrag"]);
            array_push($rekeningenLijst, $rekeningObj);
        }

        $dbh = null;
        return $rekeningenLijst;
    }

    public function ToonRekeningenViaId($id)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("SELECT * FROM rekeningen WHERE rekeningid = :rekeningid");
        $stmt->bindValue(":rekeningid", $id);
        $stmt->execute();
        $resultSet = $stmt->fetch(PDO::FETCH_ASSOC);

        $rekeningObj = new Rekening($resultSet["rekeningId"], $resultSet["naam"], $resultSet["bedrag"]);

        $dbh = null;
        return $rekeningObj;
    }

    public function ZoekRekeningViaNaam($naam)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("SELECT * FROM rekeningen WHERE naam = :naam");
        $stmt->bindValue(":naam", $naam);
        $stmt->execute();
        $resultSet = $stmt->fetch(PDO::FETCH_ASSOC);
        $dbh = null;

        if (empty($resultSet)) {
            return false;
        }else {
            return true;
        }
    }

    public function AddRekening()
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("INSERT INTO rekeningen (naam, bedrag) VALUES (:naam, :bedrag)");
        $stmt->bindValue(":naam", $this->naam);
        $stmt->bindValue(":bedrag", $this->bedrag);
        $stmt->execute();
        $laatsteNieuweId = $dbh->lastInsertId();
        $dbh = null;
        $this->rekeningId = $laatsteNieuweId;

        return $this;
    }

    public function UpdateRekening()
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("UPDATE rekeningen SET bedrag =:bedrag WHERE rekeningid = :rekeningid");
        $stmt->bindValue(":bedrag", $this->bedrag);
        $stmt->bindValue(":rekeningid", $this->rekeningId);
        $stmt->execute();
        $dbh = null;
    }

    public function DeleteRekening($id)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("DELETE FROM rekeningen WHERE rekeningid = :rekeningid");
        $stmt->bindValue(":rekeningid", $id);
        $stmt->execute();
        $dbh = null;
    }
}