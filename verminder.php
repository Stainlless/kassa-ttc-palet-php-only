<?php
include_once ("producten.php");
include_once ("rekeningen.php");
include_once ("rekeningproducten.php");

$rekeningId = $_GET["rekeningid"];
$productId = $_GET["productid"];

$rekeningProductObj = new RekeningProduct();
$rekeningProduct = $rekeningProductObj->ToonViaRekeningenProductId($rekeningId, $productId);

$aantal = $rekeningProduct->getAantal();

if ($aantal >=2) {
    $rekeningProduct->setAantal($aantal -= 1);
    $rekeningProduct->UpdateRekeningProduct();
}else{
    $rekeningProduct->DeleteRekeningProduct($rekeningProduct->getRekeningproductId());
}

$productObj = new Product();
$product = $productObj->ProductViaId($productId);
$rekeningObj = new Rekening();
$rekening = $rekeningObj->toonRekeningenViaId($rekeningId);
$bedrag = $rekening->getBedrag();
$rekening->setBedrag($bedrag -= $product->getPrijs());
$rekening->UpdateRekening();


header('location:openrekening.php?rekeningid='. $rekeningId);
exit;
