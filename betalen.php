<?php
include_once ("rekeningproducten.php");
include_once ("rekeningen.php");
$rekeningid = $_GET["rekeningid"];
$rekeningObj = new Rekening();
$rekeningProductObj = new RekeningProduct();

$rekeningProductObj->DeleteViaRekeningId($rekeningid);
$rekeningObj->DeleteRekening($rekeningid);

header("location:index.php");
exit;