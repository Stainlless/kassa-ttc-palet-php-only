<?php
include_once ("producten.php");
include_once ("rekeningproducten.php");
include_once ("rekeningen.php");
$rekeningid = $_GET["rekeningid"];
$rekeningObj = new Rekening();
$rekening = $rekeningObj->ToonRekeningenViaId($_GET['rekeningid']);
$productenObj = new Product();
$productenLijst = $productenObj->toonProducten();
$rekeningProductenObj = new RekeningProduct();
$rekeningProductenLijst = $rekeningProductenObj->ToonViaRekeningId($rekeningid);
include_once ("header.php");
?>
    <div class="row">
        <div class="columnleft">
            <div class="header">
                <?php
                    echo "<h2>".$rekening->getNaam() . " " . $rekening->getBedrag() . "€</h2>";
                ?>
            </div>
            <div class="flex-container">
                <?php
                    foreach ($productenLijst as $product) {
                        echo "<div class=\"product\" onclick=\"location.href='addorupdate.php?rekeningid=" . $rekeningid ."&productid=" . $product->getProductId() . "';\"> <img src=\"productimages/p" . $product->getProductId() . ".jpg\"> </div>";
                    }
                ?>
            </div>
        </div>
        <div class="columnright">
            <div class="lijstproducteninrekening">
                <table>
                    <?php
                    if($rekeningProductenLijst !== false) {
                        foreach ($rekeningProductenLijst as $rekeningProduct){
                            $productinfo = $productenObj->ProductViaId($rekeningProduct->getProductId());
                            echo "<tr class=\"evenoneven\" onclick=\"location.href='verminder.php?rekeningid=" . $rekeningid ."&productid=" . $rekeningProduct->getProductId() . "';\"><td>" . $productinfo->getProductNaam() . "</td><td>" . $rekeningProduct->getAantal() ."</td></tr>";
                        }
                    }
                    ?>
                </table>
            </div>
            <div>
                <?php
                echo "<div class='opties' onclick=\"location.href='index.php'\"><img src=\"productimages/exit.jpg\"> </div>";
                echo "<div class='opties' onclick=\"location.href='betalen.php?rekeningid=" . $rekeningid . " '\"><img src=\"productimages/betalen.jpg\"><div>";
                ?>
            </div>
        </div>
    </div>
<?php
include_once ("footer.php");
?>
