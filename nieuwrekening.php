<?php
include_once ("rekeningen.php");
$formingevuld = false;
$error = "";

if (isset($_POST["btnverzend"])) {
    $naam = "";
    $zoekrekeningObj = new Rekening();

    if (empty($_POST["txtNaam"])) {
        $error .= "De naam moet ingevuld worden<br>";

    }elseif ($zoekrekening = $zoekrekeningObj->zoekRekeningViaNaam($_POST["txtNaam"])) {
        $error .= "de rekening met naam " . $_POST["txtNaam"] . " bestaat al <br>";

    } else {
        $formingevuld = true;
        $naam = $_POST["txtNaam"];
    }

    if ($error == "") {

        $nieuwerekening = new Rekening();
        $nieuwerekening->setNaam($naam);
        $nieuwerekening->setBedrag(0.00);
        $nieuwerekening->AddRekening();
    }
}

include_once ("header.php");
?>
<script>

</script>
<div class="row">
    <div class="columnleft">
        <div class="header">
            <h2>Nieuwe Rekening</h2>
            <?php
            if ($error == "" && isset($nieuwerekening) && $formingevuld == true) {
                header('location:openrekening.php?rekeningid='. $nieuwerekening->getRekeningId());
                exit;
            } else if ($error !== "" && $formingevuld == false) {
                echo "<span style=\"color:red;\">" . $error . "</span>";
            }
            if (!isset($nieuwerekening))
            {
            ?>
            <form  class="formulier" action="<?php echo htmlentities($_SERVER["PHP_SELF"]); ?>" method="POST">
                <p>Naam <input class="use-keyboard-input" type="text" name="txtNaam" maxlength="64"></p>
                <input class="rekening" type="submit" value="rekening aanmaken" name="btnverzend">
            </form>
        </div>
    </div>
</div>
<?php
}
include_once ("footer.php");
?>
