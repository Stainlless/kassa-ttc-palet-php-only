<?php

require_once("dbconfig.php");
require_once("exceptions.php");

class Product
{
    private $productId;
    private $productNaam;
    private $prijs;
    private $verkrijgbaar;

    public function __construct($cproductId = null, $cproductNaam = null, $cprijs = null, $cverkrijgbaar = null)
    {
        $this->productId = $cproductId;
        $this->productNaam = $cproductNaam;
        $this->prijs = $cprijs;
        $this->verkrijgbaar = $cverkrijgbaar;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function getProductNaam()
    {
        return $this->productNaam;
    }

    public function setProductNaam($productNaam)
    {
        $this->productNaam = $productNaam;
    }

    public function getPrijs(): float
    {
        return $this->prijs;
    }

    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;
    }

    public function getVerkrijgbaar()
    {
        return $this->verkrijgbaar;
    }

    public function setVerkrijgbaar($verkrijgbaar)
    {
        $this->verkrijgbaar = $verkrijgbaar;
    }

    public function toonProducten()
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("SELECT * FROM producten WHERE verkrijgbaar = :verkrijgbaar");
        $stmt->bindValue(":verkrijgbaar", 1);
        $stmt->execute();
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultSet)) {
            throw new GeenProductenException();
        }

        $productenLijst = array();

        foreach ($resultSet as $product) {
            $productObj = new Product($product["productId"], $product["productNaam"], $product["prijs"], $product["verkrijgbaar"]);
            array_push($productenLijst, $productObj);
        }

        $dbh = null;
        return $productenLijst;
    }

    public function ProductViaId($id)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("SELECT * FROM producten WHERE productid = :productid");
        $stmt->bindValue(":productid", $id);
        $stmt->execute();
        $resultSet = $stmt->fetch(PDO::FETCH_ASSOC);

        $productObj = new Product($resultSet["productId"], $resultSet["productNaam"], $resultSet["prijs"], $resultSet["verkrijgbaar"]);

        $dbh = null;
        return $productObj;
    }
}