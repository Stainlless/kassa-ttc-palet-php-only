-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 18 nov 2021 om 14:25
-- Serverversie: 10.4.18-MariaDB
-- PHP-versie: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kassattcpaletphponly`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `producten`
--

CREATE TABLE `producten` (
  `productId` int(11) NOT NULL,
  `productNaam` varchar(64) NOT NULL,
  `prijs` decimal(4,2) NOT NULL,
  `verkrijgbaar` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `producten`
--

INSERT INTO `producten` (`productId`, `productNaam`, `prijs`, `verkrijgbaar`) VALUES
(1, 'Cola', '1.30', 1),
(2, 'Cola-Zero', '1.30', 1),
(3, 'Cola-Light', '1.30', 1),
(4, 'Fanta', '1.30', 1),
(5, 'Sprite', '1.30', 1),
(6, 'ChaudFontaine-Bruis', '1.30', 1),
(7, 'ChaudFontaine-Plat', '1.30', 1),
(8, 'Looza-Orange', '1.30', 1),
(9, 'Looza-Ace', '1.30', 1),
(10, 'Schweppes-Agrum', '1.30', 1),
(11, 'IceTea-Zero', '1.50', 1),
(12, 'Tonissteiner', '1.50', 1),
(13, 'Aquarius-Lemon', '1.50', 1),
(14, 'Aquarius-Orange', '1.50', 1),
(15, 'Aquarius-RedPeach', '1.50', 1),
(16, 'Koffie-Dessert', '1.30', 1),
(17, 'Koffie-Deca', '1.30', 1),
(18, 'Lipton-Thee', '1.30', 1),
(19, 'Cecemel-Warm', '1.30', 1),
(20, 'Royco', '1.30', 1),
(21, 'Lays-Zout', '1.20', 1),
(22, 'Lays-Paprika', '1.20', 1),
(23, 'Snoepreep', '1.20', 1),
(24, 'Crocque-Monsieur', '1.20', 1),
(25, 'Juplier', '1.30', 1),
(26, 'Cristal', '1.30', 1),
(27, 'Carlsberg', '1.30', 0),
(28, 'TerDolen-Blond', '1.80', 1),
(29, 'TerDolen-Kriek', '1.80', 1),
(30, 'Duvel', '2.50', 1),
(31, 'Training', '1.00', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `rekeningen`
--

CREATE TABLE `rekeningen` (
  `rekeningId` int(11) NOT NULL,
  `naam` varchar(64) NOT NULL,
  `bedrag` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `rekeningproducten`
--

CREATE TABLE `rekeningproducten` (
  `RekeningProductId` int(11) NOT NULL,
  `RekeningId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `aantal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `producten`
--
ALTER TABLE `producten`
  ADD PRIMARY KEY (`productId`),
  ADD UNIQUE KEY `productId` (`productId`);

--
-- Indexen voor tabel `rekeningen`
--
ALTER TABLE `rekeningen`
  ADD PRIMARY KEY (`rekeningId`);

--
-- Indexen voor tabel `rekeningproducten`
--
ALTER TABLE `rekeningproducten`
  ADD PRIMARY KEY (`RekeningProductId`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `producten`
--
ALTER TABLE `producten`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT voor een tabel `rekeningen`
--
ALTER TABLE `rekeningen`
  MODIFY `rekeningId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `rekeningproducten`
--
ALTER TABLE `rekeningproducten`
  MODIFY `RekeningProductId` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
