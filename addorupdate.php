<?php
include_once ("producten.php");
include_once ("rekeningen.php");
include_once ("rekeningproducten.php");
include_once ("dbconfig.php");

$rekeningId = $_GET["rekeningid"];
$productId = $_GET["productid"];
$status  = true;

$rekeningProductObj = new RekeningProduct();
$rekeningProduct = $rekeningProductObj->ToonViaRekeningenProductId($rekeningId, $productId);
$productObj = new Product();
$product = $productObj->ProductViaId($productId);
$prijs = $product->getPrijs();
$rekeningObj = new Rekening();
$rekening = $rekeningObj->ToonRekeningenViaId($rekeningId);
$bedrag = $rekening->getBedrag();


$dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
$dbh->beginTransaction();

if ($rekeningProduct == false) {
    $stmt = $dbh->prepare("INSERT INTO rekeningproducten (rekeningid, productid, aantal) VALUES (:rekeningid, :productid, :aantal)");
    $stmt->bindValue(":rekeningid", $rekeningId);
    $stmt->bindValue(":productid",$productId);
    $stmt->bindValue(":aantal", 1);
    $stmt->execute();
    if ($stmt->errorCode() !== "00000") {
        $status = false;
    }

}else{
    $aantal = $rekeningProduct->getAantal();
    $nieuwAantal = $aantal += 1;

    $stmt = $dbh->prepare("UPDATE rekeningproducten SET aantal = :aantal WHERE rekeningproductid = :rekeningproductid");
    $stmt->bindValue(":rekeningproductid", $rekeningProduct->getRekeningproductId());
    $stmt->bindValue(":aantal", $nieuwAantal);
    $stmt->execute();
    if ($stmt->errorCode() !== "00000") {
        $status = false;
    }

}
$nieuwBedrag = $bedrag += $prijs;

$stmt2 = $dbh->prepare("UPDATE rekeningen SET bedrag = :bedrag WHERE rekeningid = :rekeningid");
$stmt2->bindValue(":rekeningid", $rekeningId);
$stmt2->bindValue(":bedrag", $nieuwBedrag);
$stmt2->execute();
if ($stmt2->errorCode() !== "00000") {
    $status = false;
}

if ($status == true) {
    $dbh->commit();
}else {
    $dbh->rollBack();
}
$dbh = null;

header('location:openrekening.php?rekeningid='. $rekeningId);
exit;
