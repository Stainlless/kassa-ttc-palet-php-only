<?php

require_once("dbconfig.php");

class RekeningProduct
{
    private $rekeningproductId;
    private $rekeningId;
    private $productId;
    private $aantal;

    public function __construct($crekeningproductid = null, $crekeningid = null, $cproductid = null, $caantal = null)
    {
        $this->rekeningproductId = $crekeningproductid;
        $this->rekeningId = $crekeningid;
        $this->productId = $cproductid;
        $this->aantal = $caantal;
    }


    public function getRekeningproductId()
    {
        return $this->rekeningproductId;
    }

    public function getRekeningId()
    {
        return $this->rekeningId;
    }

    public function setRekeningId($rekeningId)
    {
        $this->rekeningId = $rekeningId;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    public function getAantal(): int
    {
        return $this->aantal;
    }

    public function setAantal($aantal)
    {
        $this->aantal = $aantal;
    }

    public function ToonViaRekeningId($id)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("SELECT * FROM rekeningproducten WHERE rekeningid = :rekeningid");
        $stmt->bindValue(":rekeningid", $id);
        $stmt->execute();
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultSet)) {
            Return false;
        }

        $rekeningproductLijst = array();

        foreach ($resultSet as $rekeningproduct) {
            $rekeningproductObj = new rekeningproduct($rekeningproduct["RekeningProductId"], $rekeningproduct["RekeningId"], $rekeningproduct["ProductId"], $rekeningproduct["aantal"]);
            array_push($rekeningproductLijst, $rekeningproductObj);
        }

        $dbh = null;
        return $rekeningproductLijst;
    }


    public function ToonViaRekeningenProductId($rekeningId, $productId)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("SELECT * FROM rekeningproducten WHERE rekeningid = :rekeningid AND productid = :productid");
        $stmt->bindValue(":rekeningid", $rekeningId);
        $stmt->bindValue(":productid", $productId);
        $stmt->execute();
        $resultSet = $stmt->fetch(PDO::FETCH_ASSOC);
        $dbh = null;

        if (empty($resultSet)) {
            return false;
        }else{
            $rekeningProductObj = new RekeningProduct($resultSet["RekeningProductId"], $resultSet["RekeningId"], $resultSet["ProductId"], $resultSet["aantal"]);
            return $rekeningProductObj;
        }
    }

    public function UpdateRekeningProduct(){
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("UPDATE rekeningproducten SET aantal =:aantal WHERE rekeningid = :rekeningid AND productid = :productid");
        $stmt->bindValue(":aantal", $this->aantal);
        $stmt->bindValue(":rekeningid", $this->rekeningId);
        $stmt->bindValue(":productid", $this->productId);
        $stmt->execute();
        $dbh = null;
    }

    public function DeleteViaRekeningId($rekeningId)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("DELETE FROM rekeningproducten WHERE rekeningid = :rekeningid");
        $stmt->bindValue(":rekeningid", $rekeningId);
        $stmt->execute();
        $dbh = null;
    }

    public function DeleteRekeningProduct($rekeningproductId)
    {
        $dbh = new PDO(DBconfig::$DB_CONNSTRING, DBconfig::$DB_USER, DBconfig::$DB_PASSWORD);
        $stmt = $dbh->prepare("DELETE FROM rekeningproducten WHERE rekeningproductid = :rekeningproductid");
        $stmt->bindValue(":rekeningproductid", $rekeningproductId);
        $stmt->execute();
        $dbh = null;
    }
}
