<?php
include_once ("rekeningen.php");
$rekeningenObj = new Rekening();
$rekeningenLijst = $rekeningenObj->ToonRekeningen();
include_once ("header.php");
?>
    <div class="row">

        <div class="columnleft">
            <div class="header">
                <h1>Kassa TTC Palet</h1>
            </div>
            <div class="flex-container">
                <?php
                if ($rekeningenLijst !== false) {
                    foreach ($rekeningenLijst as $rekening) {
                        echo "<div class=\"rekening\" onclick=\"location.href='openrekening.php?rekeningid=" . $rekening->getrekeningId() . "';\"" . $rekening->getRekeningId() . "\">" . $rekening->getNaam() . "<br>" . $rekening->getBedrag() . "€</div>";
                    }
                }
                ?>
                <div class="rekening" onclick=location.href="nieuwrekening.php">nieuwe rekening<br>aanmaken</div>
            </div>
        </div>
    </div>

<?php
include_once ("footer.php");
?>